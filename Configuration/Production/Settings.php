<?php

$GLOBALS['TYPO3_CONF_VARS']['SYS']['displayErrors'] = 0;
$GLOBALS['TYPO3_CONF_VARS']['SYS']['devIPmask'] = '127.0.0.1';
$GLOBALS['TYPO3_CONF_VARS']['SYS']['sqlDebug'] = 0;
$GLOBALS['TYPO3_CONF_VARS']['SYS']['enableDeprecationLog'] = 0;

