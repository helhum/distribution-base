<?php
namespace Helhum\TYPO3\CMS\Base\Distribution;
includeIfExists(__DIR__ . '/../Settings.php');

// Credentials
$GLOBALS['TYPO3_CONF_VARS']['SYS']['encryptionKey'] = 'doesnotmatteronlocaldev';
$GLOBALS['TYPO3_CONF_VARS']['BE']['installToolPassword'] = '$P$C8.8X90tS2Djh1XOCTFehnh6Xu5uAp1'; // joh316
$GLOBALS['TYPO3_CONF_VARS']['DB'] = array(
	'database' => 'kd_PROJECTID',
	'host' => 'localhost',
	'password' => 'root',
	'port' => 3306,
	'username' => 'root',
);

// Dev Settings


// Host Settings
$GLOBALS['TYPO3_CONF_VARS']['GFX']['im_path'] = '/opt/local/bin/';
$GLOBALS['TYPO3_CONF_VARS']['GFX']['im_path_lzw'] = '/opt/local/bin/';
